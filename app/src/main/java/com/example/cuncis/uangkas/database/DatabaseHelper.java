package com.example.cuncis.uangkas.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "uangkas.db";
    private static final int DB_VERSION = 1;

    public static final String DB_TABLE = "transaksi";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + DB_TABLE +" (transaksi_id INTEGER PRIMARY KEY AUTOINCREMENT, status TEXT," +
                "jumlah LONG, keterangan TEXT, tanggal DATE DEFAULT CURRENT_DATE);";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE);
    }
}
