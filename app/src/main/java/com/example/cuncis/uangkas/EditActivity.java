package com.example.cuncis.uangkas;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.cuncis.uangkas.database.Config;
import com.example.cuncis.uangkas.database.DatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

public class EditActivity extends AppCompatActivity {
    private static final String TAG = "EditActivity";

    RadioGroup radioStatus;
    RadioButton radioMasuk, radioKeluar;
    EditText editJumlah, editKeterangan, editTanggal;
    Button btnSimpan;
    RippleView ripSimpan;

    String status, tanggal;

    DatabaseHelper dbHelper;
    Cursor cursor;      //menentukan kolom keberapa

    DatePickerDialog datePickerDialog;

    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH);
    int day = calendar.get(Calendar.DAY_OF_MONTH);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        getSupportActionBar().setTitle("Edit");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        status = "";    //for saved date to sqlite = yyyy-mm-dd

        radioStatus     = findViewById(R.id.radio_status);
        radioMasuk      = findViewById(R.id.radio_masuk);
        radioKeluar     = findViewById(R.id.radio_keluar);
        editJumlah      = findViewById(R.id.edit_jumlah);
        editKeterangan  = findViewById(R.id.edit_keterangan);
        editTanggal     = findViewById(R.id.edit_tanggal);
        btnSimpan       = findViewById(R.id.btn_simpan);
        ripSimpan       = findViewById(R.id.rip_simpan);

        _editMysql();

    }

    private void simpanEdit() {
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        db.execSQL("UPDATE " + DatabaseHelper.DB_TABLE + " SET status=" +
//                "'" + status + "', jumlah='" + editJumlah.getText().toString().trim() + "', keterangan=" +
//                "'" + editKeterangan.getText().toString().trim() + "', tanggal='" + tanggal +
//                "' WHERE transaksi_id='" + MainActivity.transaksiId + "'");

        AndroidNetworking.post(Config.HOST + "update.php")
                .addBodyParameter("transaksi_id", MainActivity.transaksiId)
                .addBodyParameter("status", status)
                .addBodyParameter("jumlah", editJumlah.getText().toString())
                .addBodyParameter("keterangan", editKeterangan.getText().toString())
                .addBodyParameter("tanggal", tanggal)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            if (response.getString("response").equals("Success")) {
                                Toast.makeText(EditActivity.this, "Perubahan berhasil disimpan :)", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(EditActivity.this, response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });

        finish();       //activity akan menutup otomatis dan kembali ke MainActivity
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void _editSqlite() {
        dbHelper = new DatabaseHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT *, strftime('%d/%m/%Y', tanggal) AS tgl FROM " + DatabaseHelper.DB_TABLE
                + " WHERE transaksi_id = '" + MainActivity.transaksiId + "'", null);
        cursor.moveToFirst();

        status = cursor.getString(1);
        switch (status) {
            case "MASUK":
                radioMasuk.setChecked(true);
                radioKeluar.setEnabled(false);
                break;
            case "KELUAR":
                radioKeluar.setChecked(true);
                radioMasuk.setEnabled(false);
                break;
        }

        editJumlah.setText(cursor.getString(2));
        editKeterangan.setText(cursor.getString(3));

        tanggal = cursor.getString(4);
        editTanggal.setText(cursor.getString(5));
        editTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog = new DatePickerDialog(EditActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        NumberFormat numberFormat = new DecimalFormat("00");

                        tanggal = year + "-" + numberFormat.format(month + 1) + "-" + numberFormat.format(dayOfMonth);
                        Log.i(TAG, "onDateSet: tanggal: " + tanggal);
                        //data yg akan diambil
                        editTanggal.setText(numberFormat.format(dayOfMonth) + "/" + numberFormat.format(month+1)
                                + "/" + year);
                    }
                }, year, month, day);   //data yang ingin dimunculkan
                datePickerDialog.show();
            }
        });

        ripSimpan.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                if (status.equals("") ||  editJumlah.getText().toString().trim().equals("")
                        || editKeterangan.getText().toString().trim().equals("")) {
                    Toast.makeText(getApplicationContext(), "Isi data dengan benar :(", Toast.LENGTH_SHORT).show();
                }else if (status.equals("")) {
                    Toast.makeText(getApplicationContext(), "Status harus diisi", Toast.LENGTH_SHORT).show();
                } else if (editJumlah.getText().toString().trim().equals("")) {
                    Toast.makeText(getApplicationContext(), "Jumlah harus diisi", Toast.LENGTH_SHORT).show();
                } else if (editKeterangan.getText().toString().trim().equals("")) {
                    Toast.makeText(getApplicationContext(), "Keterangan harus diisi", Toast.LENGTH_SHORT).show();
                } else {
                    simpanEdit();
                }
            }
        });
    }

    private void _editMysql() {
        status = MainActivity.status;
        switch (status) {
            case "MASUK":
                radioMasuk.setChecked(true);
                radioKeluar.setEnabled(false);
                break;
            case "KELUAR":
                radioKeluar.setChecked(true);
                radioMasuk.setEnabled(false);
                break;
        }

        editJumlah.setText(MainActivity.jumlah);
        editKeterangan.setText(MainActivity.keterangan);

        tanggal = MainActivity.tanggal2;
        editTanggal.setText(MainActivity.tanggal);
        editTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog = new DatePickerDialog(EditActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        NumberFormat numberFormat = new DecimalFormat("00");

                        tanggal = year + "-" + numberFormat.format(month + 1) + "-" + numberFormat.format(dayOfMonth);
                        Log.i(TAG, "onDateSet: tanggal: " + tanggal);
                        //data yg akan diambil
                        editTanggal.setText(numberFormat.format(dayOfMonth) + "/" + numberFormat.format(month+1)
                                + "/" + year);
                    }
                }, year, month, day);   //data yang ingin dimunculkan
                datePickerDialog.show();
            }
        });

        ripSimpan.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                if (status.equals("") ||  editJumlah.getText().toString().trim().equals("")
                        || editKeterangan.getText().toString().trim().equals("")) {
                    Toast.makeText(getApplicationContext(), "Isi data dengan benar :(", Toast.LENGTH_SHORT).show();
                }else if (status.equals("")) {
                    Toast.makeText(getApplicationContext(), "Status harus diisi", Toast.LENGTH_SHORT).show();
                } else if (editJumlah.getText().toString().trim().equals("")) {
                    Toast.makeText(getApplicationContext(), "Jumlah harus diisi", Toast.LENGTH_SHORT).show();
                } else if (editKeterangan.getText().toString().trim().equals("")) {
                    Toast.makeText(getApplicationContext(), "Keterangan harus diisi", Toast.LENGTH_SHORT).show();
                } else {
                    simpanEdit();
                }
            }
        });
    }

}
