package com.example.cuncis.uangkas.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.cuncis.uangkas.AddActivity;
import com.example.cuncis.uangkas.R;
import com.example.cuncis.uangkas.database.Config;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class MasukFragment extends Fragment {

    EditText editJumlah, editKeterangan;
    RippleView ripSimpan;

    public MasukFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_masuk, container, false);

        editJumlah = view.findViewById(R.id.edit_jumlah);
        editKeterangan = view.findViewById(R.id.edit_keterangan);
        ripSimpan = view.findViewById(R.id.rip_simpan);

        ripSimpan.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                if (editJumlah.getText().toString().trim().equals("")
                        || editKeterangan.getText().toString().trim().equals("")) {
                    Toast.makeText(getActivity(), "Isi data dengan benar :(", Toast.LENGTH_SHORT).show();
                } else if (editJumlah.getText().toString().trim().equals("")) {
                    Toast.makeText(getActivity(), "Jumlah harus diisi", Toast.LENGTH_SHORT).show();
                } else if (editKeterangan.getText().toString().trim().equals("")) {
                    Toast.makeText(getActivity(), "Keterangan harus diisi", Toast.LENGTH_SHORT).show();
                } else {
                    _createMysql();
                }
            }
        });

        return view;
    }

    private void _createMysql() {
        AndroidNetworking.post(Config.HOST + "create.php")
                .addBodyParameter("status", "MASUK")
                .addBodyParameter("jumlah", editJumlah.getText().toString())
                .addBodyParameter("keterangan", editKeterangan.getText().toString())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            if (response.getString("response").equals("Success")) {
                                Toast.makeText(getActivity(), "Transaksi berhasil disimpan :)", Toast.LENGTH_SHORT).show();
                                getActivity().finish();
                            } else {
                                Toast.makeText(getActivity(), response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

}
