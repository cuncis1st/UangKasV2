package com.example.cuncis.uangkas;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.cuncis.uangkas.database.Config;
import com.example.cuncis.uangkas.database.DatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddActivity extends AppCompatActivity {

    @BindView(R.id.radio_status) RadioGroup radioStatus;
    @BindView(R.id.radio_masuk) RadioButton radioMasuk;
    @BindView(R.id.radio_keluar) RadioButton radioKeluar;
    @BindView(R.id.edit_jumlah) EditText editJumlah;
    @BindView(R.id.edit_keterangan) EditText editKeterangan;
    @BindView(R.id.rip_simpan) RippleView ripSimpan;

    String status;

    DatabaseHelper dbHelper;
    Cursor cursor;      //menentukan kolom keberapa

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        ButterKnife.bind(this);

        status = "";
        dbHelper = new DatabaseHelper(this);

        radioStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_masuk:
                        status = "MASUK";
                        break;
                    case R.id.radio_keluar:
                        status = "KELUAR";
                        break;
                }
            }
        });

        ripSimpan.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                if (status.equals("") ||  editJumlah.getText().toString().trim().equals("")
                        || editKeterangan.getText().toString().trim().equals("")) {
                    Toast.makeText(AddActivity.this, "Isi data dengan benar :(", Toast.LENGTH_SHORT).show();
                }else if (status.equals("")) {
                    Toast.makeText(AddActivity.this, "Status harus diisi", Toast.LENGTH_SHORT).show();
                } else if (editJumlah.getText().toString().trim().equals("")) {
                    Toast.makeText(AddActivity.this, "Jumlah harus diisi", Toast.LENGTH_SHORT).show();
                } else if (editKeterangan.getText().toString().trim().equals("")) {
                    Toast.makeText(AddActivity.this, "Keterangan harus diisi", Toast.LENGTH_SHORT).show();
                } else {
                    _createMysql();
                }
            }
        });

        getSupportActionBar().setTitle("Tambah");   //tambah judulnya
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  //tambahin panah *back

    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();   //untuk aksinya (menutup halaman yg kita buka)
        return true;
    }

    private void _createMysql() {
        AndroidNetworking.post(Config.HOST + "create.php")
                .addBodyParameter("status", status)
                .addBodyParameter("jumlah", editJumlah.getText().toString())
                .addBodyParameter("keterangan", editKeterangan.getText().toString())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            if (response.getString("response").equals("Success")) {
                                Toast.makeText(AddActivity.this, "Transaksi berhasil disimpan :)", Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                Toast.makeText(AddActivity.this, response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void _createSqlite() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("INSERT INTO " + DatabaseHelper.DB_TABLE + "(status, jumlah, keterangan) VALUES (" +
                "'" + status + "', '" + editJumlah.getText().toString().trim() + "', " +
                "'" + editKeterangan.getText().toString().trim() + "')");

        Toast.makeText(this, "Transaksi berhasil disimpan :)", Toast.LENGTH_SHORT).show();
        finish();       //activity akan menutup otomatis dan kembali ke MainActivity
    }





}
