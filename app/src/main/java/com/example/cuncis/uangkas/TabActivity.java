package com.example.cuncis.uangkas;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.cuncis.uangkas.fragment.KeluarFragment;
import com.example.cuncis.uangkas.fragment.MasukFragment;

public class TabActivity extends AppCompatActivity {

    private final String[] PAGE_TITLES = new String[]{
            "PEMASUKAN", "PENGELUARAN"
    };

    private final Fragment[] PAGES = new Fragment[]{
            new MasukFragment(), new KeluarFragment()
    };

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);

        viewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);

        getSupportActionBar().setTitle("Tambah Baru");   //tambah judulnya
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  //tambahin panah *back
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();   //untuk aksinya (menutup halaman yg kita buka)
        return true;
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return PAGES[position];
        }

        @Override
        public int getCount() {
            return PAGES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return PAGE_TITLES[position];
        }

    }
}
