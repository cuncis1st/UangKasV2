package com.example.cuncis.uangkas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.cuncis.uangkas.database.Config;
import com.example.cuncis.uangkas.database.DatabaseHelper;
import com.leavjenn.smoothdaterangepicker.date.SmoothDateRangePickerFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    TextView textMasuk, textKeluar, textTotal;
    ListView listKas;
    SwipeRefreshLayout swipeRefresh;

    DatabaseHelper dbHelper;
    Cursor cursor;      //menentukan kolom keberapa

    ArrayList<HashMap<String, String>> arusKas;

    String queryKas, queryTotal;
    public static boolean filter;

    public static String transaksiId, tgl_dari, tgl_ke;
    public static TextView textFilter;

    public static String link, status, keterangan, jumlah, tanggal, tanggal2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startActivity(new Intent(MainActivity.this, IntroActivity.class));

        swipeRefresh = findViewById(R.id.swipe_refresh);
        textMasuk = findViewById(R.id.text_masuk);
        textKeluar = findViewById(R.id.text_keluar);
        textTotal = findViewById(R.id.text_saldo);
        listKas = findViewById(R.id.listView);
        textFilter = findViewById(R.id.text_filter);
        arusKas = new ArrayList<>();

        textFilter.setText("SEMUA");

        FloatingActionButton floatingActionButton = findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Fab button clicked", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(MainActivity.this, AddActivity.class));
                startActivity(new Intent(MainActivity.this, TabActivity.class));
            }
        });

        dbHelper = new DatabaseHelper(this);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                queryKas = "SELECT *, strftime('%d/%m/%Y', tanggal) AS tgl FROM " + DatabaseHelper.DB_TABLE + " ORDER BY transaksi_id DESC";
                queryTotal = "SELECT SUM (jumlah) AS total," +
                        "(SELECT SUM (jumlah) FROM transaksi WHERE status='MASUK') AS masuk," +
                        "(SELECT SUM (jumlah) FROM transaksi WHERE status='KELUAR') AS keluar FROM transaksi";
//                kasAdapter();
                link = Config.HOST + "read.php";
                _readMysql();
                textFilter.setText("SEMUA");
            }
        });
    }

    private void _deleteMysql() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Konfirmasi");
        builder.setMessage("Apa kamu yakin?");
        builder.setPositiveButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                AndroidNetworking.post(Config.HOST + "delete.php")
                        .addBodyParameter("transaksi_id", transaksiId)
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // do anything with response
                                try {
                                    if (response.getString("response").equals("Success")) {
                                        Toast.makeText(MainActivity.this, "Data berhasil dihapus", Toast.LENGTH_SHORT).show();
                                        _readMysql();
                                    } else {
                                        Toast.makeText(MainActivity.this, response.getString("response"), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                            }
                        });

            }
        });
        builder.show();
    }

    private void listMenu() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setTitle("pilih: ");
        dialog.setContentView(R.layout.list_menu);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        RippleView ripHapus = dialog.findViewById(R.id.rip_hapus);
        RippleView ripEdit = dialog.findViewById(R.id.rip_edit);

        ripHapus.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                dialog.dismiss();
//                hapus();
                _deleteMysql();
            }
        });

        ripEdit.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                dialog.dismiss();
                edit();
            }
        });

        dialog.show();
    }

    private void edit() {
        startActivity(new Intent(MainActivity.this, EditActivity.class));
    }

    private void hapus() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Konfirmasi");
        builder.setMessage("Apa kamu yakin?");
        builder.setPositiveButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("DELETE FROM " + DatabaseHelper.DB_TABLE + " WHERE transaksi_id = '" + transaksiId + "'");

                kasAdapter();
                Toast.makeText(MainActivity.this, "Data berhasil dihapus", Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }

    @Override
    protected void onResume() {     //ketika activity masuk -> terus diapain
        super.onResume();

        queryKas = "SELECT *, strftime('%d/%m/%Y', tanggal) AS tgl FROM " + DatabaseHelper.DB_TABLE + " ORDER BY transaksi_id DESC";
        queryTotal = "SELECT SUM (jumlah) AS total," +
                "(SELECT SUM (jumlah) FROM transaksi WHERE status='MASUK') AS masuk," +
                "(SELECT SUM (jumlah) FROM transaksi WHERE status='KELUAR') AS keluar FROM transaksi";

        if (filter) {
            queryKas = "SELECT *, strftime('%d/%m/%Y', tanggal) AS tgl FROM " + DatabaseHelper.DB_TABLE + " " +
                    "WHERE (tanggal >= '" + tgl_dari + "') AND (tanggal <= '" + tgl_ke + "') ORDER BY transaksi_id DESC";
            queryTotal = "SELECT SUM (jumlah) AS total," +
                    "(SELECT SUM (jumlah) FROM transaksi WHERE status='MASUK' AND (tanggal >= '" + tgl_dari + "') AND (tanggal <= '" + tgl_ke + "')) AS masuk," +
                    "(SELECT SUM (jumlah) FROM transaksi WHERE status='KELUAR' AND (tanggal >= '" + tgl_dari + "') AND (tanggal <= '" + tgl_ke + "')) AS keluar FROM transaksi " +
                    "WHERE (tanggal >= '" + tgl_dari + "') AND (tanggal <= '" + tgl_ke + "')";

            link = Config.HOST + "filter.php?dari=" + tgl_dari + "&ke=" + tgl_ke;
        } else {
            link = Config.HOST + "read.php";
        }

//        kasAdapter();
        _readMysql();
    }

    private void _readMysql() {
        swipeRefresh.setRefreshing(false);
        arusKas.clear();       //dikosongi dulu agar tidak menumpuk
        listKas.setAdapter(null);
        Log.d(TAG, "onResume: -> " + link);
        AndroidNetworking.post(link)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            NumberFormat rupiah = NumberFormat.getInstance(Locale.GERMANY);

                            textMasuk.setText("Rp. " + rupiah.format(response.getDouble("masuk")));
                            textKeluar.setText("Rp. " + rupiah.format(response.getDouble("keluar")));
                            textTotal.setText("Rp. " + rupiah.format(response.getDouble("masuk")
                                    - response.getDouble("keluar")));

                            JSONArray jsonArray = response.getJSONArray("hasil");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                HashMap<String, String> map = new HashMap<>();
                                map.put("transaksi_id", jsonObject.getString("transaksi_id"));
                                map.put("status", jsonObject.getString("status"));
                                map.put("jumlah", jsonObject.getString("jumlah"));
                                map.put("keterangan", jsonObject.getString("keterangan"));
                                map.put("tanggal", jsonObject.getString("tanggal"));
                                map.put("tanggal2", jsonObject.getString("tanggal2"));

                                arusKas.add(map);
                            }

                            _readAdapter();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void _readAdapter() {
        SimpleAdapter adapter = new SimpleAdapter(this,
                arusKas,
                R.layout.list_kas,
                new String[]{"transaksi_id", "status", "jumlah", "keterangan", "tanggal", "tanggal2"},
                new int[]{R.id.text_transaksiId, R.id.text_status, R.id.text_jumlah, R.id.text_keterangan,
                        R.id.text_tanggal, R.id.text_tanggal2});

        listKas.setAdapter(adapter);
        listKas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                transaksiId = ((TextView) view.findViewById(R.id.text_transaksiId)).getText().toString();
                status = ((TextView) view.findViewById(R.id.text_status)).getText().toString();
                jumlah = ((TextView) view.findViewById(R.id.text_jumlah)).getText().toString();
                keterangan = ((TextView) view.findViewById(R.id.text_keterangan)).getText().toString();
                tanggal = ((TextView) view.findViewById(R.id.text_tanggal)).getText().toString();
                tanggal2 = ((TextView) view.findViewById(R.id.text_tanggal2)).getText().toString();

                listMenu();
            }
        });
    }

    private void kasTotal() {
        NumberFormat rupiah = NumberFormat.getInstance(Locale.GERMANY);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery(queryTotal,
                null);
        cursor.moveToFirst();

        textMasuk.setText(rupiah.format(cursor.getDouble(1)));
        textKeluar.setText(rupiah.format(cursor.getDouble(2)));
        textTotal.setText(rupiah.format(cursor.getDouble(1) - cursor.getDouble(2)));

        swipeRefresh.setRefreshing(false);  //untuk menghilangkan refresh
        if (!filter) {
            textFilter.setVisibility(View.GONE);
        }

        filter = false;
    }

    private void kasAdapter() {
        arusKas.clear();       //dikosongi dulu agar tidak menumpuk
        listKas.setAdapter(null);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery(queryKas, null);
        //adanya tgl, maka terhitung sebagai index baru
        cursor.moveToFirst();   //mulai dari awal

        for (int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);

            HashMap<String, String> map = new HashMap<>();
            map.put("transaksi_id", cursor.getString(0));
            map.put("status", cursor.getString(1));
            map.put("jumlah", cursor.getString(2));
            map.put("keterangan", cursor.getString(3));
//            map.put("tanggal", cursor.getString(4));
            map.put("tanggal", cursor.getString(5));

            arusKas.add(map);
        }

        SimpleAdapter adapter = new SimpleAdapter(this,
                arusKas,
                R.layout.list_kas,
                new String[]{"transaksi_id", "status", "jumlah", "keterangan", "tanggal"},
                new int[]{R.id.text_transaksiId, R.id.text_status, R.id.text_jumlah, R.id.text_keterangan, R.id.text_tanggal});

        listKas.setAdapter(adapter);
        listKas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                transaksiId = ((TextView) view.findViewById(R.id.text_transaksiId)).getText().toString();
                Log.i(TAG, "onItemLongClick: transaksiId: " + transaksiId);

                listMenu();
            }
        });
//        listKas.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                transaksiId = ((TextView) view.findViewById(R.id.text_transaksiId)).getText().toString();
//                Log.i(TAG, "onItemLongClick: transaksiId: " + transaksiId);
//
//                listMenu();
//
//                return true;
//            }
//        });

        kasTotal();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.filter_item) {
//            startActivity(new Intent(MainActivity.this, FilterActivity.class));
            _filterMysql();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void _filterMysql() {
        SmoothDateRangePickerFragment smoothDateRangePickerFragment = SmoothDateRangePickerFragment.newInstance(
                new SmoothDateRangePickerFragment.OnDateRangeSetListener() {
                    @Override
                    public void onDateRangeSet(SmoothDateRangePickerFragment view,
                                               int yearStart, int monthStart,
                                               int dayStart, int yearEnd,
                                               int monthEnd, int dayEnd) {
                        // grab the date range, do what you want
                        tgl_dari = String.valueOf(yearStart) + "-" + String.valueOf(monthStart + 1) + "-"
                                + String.valueOf(dayStart);
                        tgl_ke = String.valueOf(yearEnd) + "-" + String.valueOf(monthEnd + 1) + "-"
                                + String.valueOf(dayEnd);
                        Log.d(TAG, "onDateRangeSet: dayStart->" + tgl_dari + ", dayEnd->" + tgl_ke);

                        textFilter.setText(
                                String.valueOf(dayStart) + "-" + String.valueOf(monthStart + 1) + "-"
                                        + String.valueOf(yearStart) +
                                        " - " +
                                        String.valueOf(dayEnd) + "-" + String.valueOf(monthEnd + 1) + "-"
                                        + String.valueOf(yearEnd)
                        );

                        link = Config.HOST + "filter.php?dari=" + tgl_dari + "&ke=" + tgl_ke;
                        _readMysql();

                    }

                });

        smoothDateRangePickerFragment.show(getFragmentManager(), "smoothDateRangePicker");
    }
}

















